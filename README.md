# jfxs / hello world

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)
[![Pipeline Status](https://gitlab.com/fxs/docker-hello-world/badges/master/pipeline.svg)](https://gitlab.com/fxs/docker-hello-world/pipelines)
[![Image size](https://fxs.gitlab.io/docker-hello-world/docker.svg)](https://hub.docker.com/r/jfxs/hello-world)

A lightweight automatically updated and tested "Hello world" multiarch amd64 and arm64 Docker image.

![Hello world screenshot](https://gitlab.com/fxs/docker-hello-world/-/raw/master/hello.png)

## Getting Started

### Prerequisities

In order to run this container you'll need Docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

Example to run this image:

```shell
docker run -d -p 8080:8080 jfxs/hello-world
```


## Built with

Docker latest tag contains:

See versions on [Dockerhub](https://hub.docker.com/r/jfxs/hello-world)

Versions of installed software are listed in /etc/VERSIONS file of the Docker image. Example to see them for a specific tag:

```shell
docker run -t jfxs/hello-world:1.18.0-1 cat /etc/VERSIONS
```

## Versioning

The Docker tag is defined by the nginx version used and an increment to differentiate build with the same nginx version:

```text
<nginx_version>-<increment>
```

Example: 1.18.0-1

## Vulnerability Scan

The Docker image is scanned every day with the open source vulnerability scanner [Trivy](https://github.com/aquasecurity/trivy).

The latest vulnerability scan report is available on [Gitlab Security Dashboard](https://gitlab.com/fxs/docker-hello-world/-/security/dashboard/?state=DETECTED&state=CONFIRMED&reportType=CONTAINER_SCANNING).

## Find Us

* [Dockerhub](https://hub.docker.com/r/jfxs/ansible)
* [Gitlab](https://gitlab.com/fxs/docker-ansible)

## Authors

* **FX Soubirou** - *Initial work* - [Gitlab repositories](https://gitlab.com/users/fxs/projects)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/docker-ansible/blob/master/LICENSE) file for details.
