
DOCKER = docker

IMAGE_CI_TOOLKIT = jfxs/ci-toolkit
IMAGE_HADOLINT  = hadolint/hadolint:latest
IMAGE_FROM_REPOSITORY = library/nginx
IMAGE_FROM_TAG = mainline-alpine
IMAGE_BUILT = hello-world

## Docker
## ------
docker-build-push: ## Build docker image. Arguments: user=dockerhubUser password=dockerhubPassword [arch=arm64] [vcs_ref=c780b3a] [tag=myTag]
docker-build-push: docker-rmi
	test -n "${user}"  # Failed if user not set
	test -n "${password}"  # Failed if password not set
	$(eval arch := $(shell if [ -z ${arch} ]; then echo "amd64"; else echo "${arch}"; fi))
	$(eval sha_tag := $(shell if [ -z ${container} ]; then \
		docker run -t ${IMAGE_CI_TOOLKIT} /bin/sh -c "get-identical-tag.sh -r ${IMAGE_FROM_REPOSITORY} -u ${user} -p ${password} -t ${IMAGE_FROM_TAG} -a ${arch} -x '\([0-9]*\.[0-9]*\.[0-9]*\)-alpine'"; \
	else \
		get-identical-tag.sh -r ${IMAGE_FROM_REPOSITORY} -u ${user} -p ${password} -t ${IMAGE_FROM_TAG} -a ${arch} -x "\([0-9]*\.[0-9]*\.[0-9]*\)-alpine"; \
	fi))
	@echo ${sha_tag}
	$(eval latest_sha := $(shell echo ${sha_tag} | awk '{print $$1}'))
	$(eval version := $(shell echo ${sha_tag} | awk '{print $$3}'))
	$(eval tag := $(shell if [ -z ${tag} ]; then echo "${IMAGE_BUILT}:latest"; else echo "${tag}"; fi))
	$(eval build_date := $(shell date -u +'%Y-%m-%dT%H:%M:%SZ'))
	if [ "${arch}" = "arm64" ]; then \
		docker buildx build --progress plain --no-cache --platform linux/arm64 --build-arg IMAGE_FROM_SHA="nginx@${latest_sha}" --build-arg NGINX_VERSION=${version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} --push . && \
		docker pull ${tag}; \
	else \
		docker build --no-cache --build-arg IMAGE_FROM_SHA="nginx@${latest_sha}" --build-arg NGINX_VERSION=${version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} . && \
		docker push ${tag}; \
	fi

docker-rm: ## Remove all unused containers
docker-rm:
	$(DOCKER) container prune -f

docker-rmi: ## Remove all untagged images
docker-rmi: docker-rm
	$(DOCKER) image prune -f

dockerhub-tag: ## Tag image on Dockerhub. Arguments: init-image-tag publish-image publish-tag
dockerhub-tag:
	test -n "${init-image-tag}"  # Failed if init-image-tag not set
	test -n "${publish-image}"  # Failed if publish-image not set
	test -n "${publish-tag}"  # Failed if publish-tag not set
	$(DOCKER) pull ${init-image-tag}
	$(DOCKER) tag ${init-image-tag} ${publish-image}:${publish-tag}
	$(DOCKER) push ${publish-image}:${publish-tag}
	$(DOCKER) tag ${init-image-tag} ${publish-image}:latest
	$(DOCKER) push ${publish-image}:latest

PHONY: docker-build-push docker-rm docker-rmi dockerhub-tag


## Tests
## ------
checks: ## Run linter checks
checks:
	$(DOCKER) run -t --rm -v "${PWD}:/mnt" ${IMAGE_HADOLINT} hadolint /mnt/Dockerfile

sanity-test: ## Run local minimal tests. Arguments: url=http://localhost:8080
sanity-test:
	test -n "${url}"  # Failed if url not set
	@if [[ $(shell curl -s "${url}" | grep -c "Hello world") != 3 ]]; then echo "Sanity test failed !" && exit 1; else echo "Sanity test 1: OK"; fi
	@if [[ $(shell curl -s "${url}" | grep -c "Server address") != 1 ]]; then  echo "Sanity test failed !" && exit 1;  else echo "Sanity test 2: OK"; fi
	@echo "Sanity test passed !"

PHONY: checks sanity-test

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
